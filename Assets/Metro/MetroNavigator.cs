using System;
using System.Collections.Generic;
using System.Linq;
using Metro;
using TMPro;
using UnityEngine;

public class MetroNavigator : MonoBehaviour
{
    [field: SerializeField] private List<StationConfiguration> Configurations { get; set; }

    [field: SerializeField] private TMP_Dropdown StartPoint { get;  set; }
    [field: SerializeField] private TMP_Dropdown EndPoint { get;  set; }

    [SerializeField]private StationConfiguration startPoint;
    [SerializeField]private StationConfiguration endPoint;
    [SerializeField] private List<Way> findedWays;

    private void Start()
    {
        Initialize();
    }
    private void Initialize()
    {
        StartPoint.ClearOptions();
        EndPoint.ClearOptions();
        foreach (EnumStationIndex index in Enum.GetValues(typeof(EnumStationIndex)))
        {
            TMP_Dropdown.OptionData data = new TMP_Dropdown.OptionData();
            data.text = $"Station {index}";
            StartPoint.options.Add(data);
            EndPoint.options.Add(data);
        }
    }

    public void OnFindWay()
    {
         Debug.Log(StartPoint.captionText.text);
         //нужно пройтись по всем поинтам найти старт и конец
         //Потом нужно выстроить ветки от старта
         //Условно A - A.b A.c
         //
         //
         //
         foreach (var config in Configurations)
         {
             Debug.Log("indexes="+config.StationIndex);
             if ($"Station {config.StationIndex}" == StartPoint.captionText.text)
             {
                 Debug.Log("i=i");
                 startPoint = config;
             }

             if ($"Station {config.StationIndex}" == EndPoint.captionText.text)
             {
                 Debug.Log("q=q");
                 endPoint = config;
             }
         }

         startPoint.Neighbors.ForEach(neighbor =>
         {
             Way tempWay = new();
             tempWay.points = new List<StationConfiguration>();
             tempWay.points.Add(startPoint);
             tempWay.points.Add(neighbor);
             GetWay(tempWay);
         });

         Debug.Log("Finded a ways =" + findedWays.Count);
         findedWays.ForEach(way =>
         {
             string str = "way =";
             way.points.ForEach(point=>str+=$"{point.StationIndex}:");
             str += $"{way.points.Count} Length";
             Debug.Log(str);
         });
    }

    private void GetWay(Way way)
    {
        foreach (var neighbor in way.points.Last().Neighbors)
        {
            var duplicate = way.points.Find(_ => _ == neighbor);
            if (duplicate == null)
            {
                Debug.Log("find");
                Way tempWay = new();
                tempWay.points = new List<StationConfiguration>();
                tempWay.points.Add(neighbor);

                if (neighbor == endPoint)
                {
                    findedWays.Add(tempWay);
                    Debug.Log($"finded count={tempWay.points.Count}");
                    return;
                }

                //recursion
                GetWay(tempWay);
            }
        }
    }
    
    

    [System.Serializable]
    public class Way
    {
        public List<StationConfiguration> points;
    }
}
