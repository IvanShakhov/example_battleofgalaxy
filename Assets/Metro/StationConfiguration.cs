using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Metro
{
    [CreateAssetMenu(fileName = "StationPoint")]
    public class StationConfiguration : ScriptableObject
    {
        [field: SerializeField] public EnumStationIndex StationIndex { get; private set; }
        [field: SerializeField] public List<EnumStationColor> StationColors { get; private set; }
        [field: SerializeField] public List<StationConfiguration> Neighbors { get; private set; }
    }
}
